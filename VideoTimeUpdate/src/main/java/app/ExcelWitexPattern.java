package app;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;

public class ExcelWitexPattern {

    private HSSFWorkbook workbook;
    private HSSFSheet worksheet;

    private int rowNr = 1;

    public ExcelWitexPattern() {
        workbook = new HSSFWorkbook();
        worksheet = workbook.createSheet();
        addHeaders();
    }

    private void addHeaders(){
        HSSFRow headerRow =  worksheet.createRow(0);
        String[] headers = ("N,Start time,Duration,Type,Category,Title,Star,Notes,Location").split(",");
        for(int x = 0; x<headers.length; x++){
            HSSFCell headerCell = headerRow.createCell(x);
            headerCell.setCellValue(headers[x]);
        }
    }

    public void addRowWithData(String data){
        HSSFRow headerRow =  worksheet.createRow(rowNr);
        String[] headers = data.split(",");
        for(int x = 0; x<headers.length; x++){
            HSSFCell headerCell = headerRow.createCell(x);
            headerCell.setCellValue(headers[x]);
        }
        rowNr++;
    }

    public void saveExcelFile(FileOutputStream fileOutputStream) throws IOException {
        workbook.write(fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }
}
